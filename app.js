//jshint esversion:6

require('dotenv').config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const encrypt = require("mongoose-encryption"); //Level 2.


const app = express();

//console.log(process.env.API_KEY);
app.use(express.static("public"));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));

mongoose.connect("mongodb://localhost:27017/userDB", {userNewUrlParser: true});

const userSchema = new mongoose.Schema ({
  email: String,
  password: String
});

//Level 2. Encrypting our DB.

userSchema.plugin(encrypt, {secret: process.env.SECRET, encryptedFields: ['password'] }); //Before Mongoose model

const User = new mongoose.model("User", userSchema);

//View our website
app.get("/", function(req, res){
  res.render("home");
});

app.get("/login", function(req, res){
  res.render("login");
});

app.get("/register", function(req, res){
  res.render("register");
});

//Making post in your register page
app.post("/register", function(req, res){
  const newUser = new User({
    email: req.body.username,
    password: req.body.password
  });

   newUser.save(function(err){
     if(err) {
       console.log(err);
     } else {
       res.render("secrets");//Rendering Secret route after Register or Login
     }
   });
});

//Making post method with login page

app.post("/login", function(req, res){
  const username = req.body.username;
  const password = req.body.password;

  User.findOne({email: username}, function(err, foundUser){
    if(err){
      console.log(err);
    } else {
      if(foundUser){
        if(foundUser.password === password){
          res.render("secrets");
        }
      }
    }
  });
});



app.listen(3000, function(){
  console.log("Server started on port 3000");
});
